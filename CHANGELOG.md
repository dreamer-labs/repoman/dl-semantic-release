# [1.1.0](https://gitlab.com/dreamer-labs/dl-semantic-release/compare/v1.0.0...v1.1.0) (2019-08-21)


### Features

* Add wrapper for semantic-release ([3f97332](https://gitlab.com/dreamer-labs/dl-semantic-release/commit/3f97332))

# 1.0.0 (2019-08-02)


### Features

* initial release ([1409705](https://gitlab.com/dreamer-labs/dl-semantic-release/commit/1409705))
