FROM node:8-alpine

COPY shipit /usr/local/bin
RUN npm install -g semantic-release \
    && npm install @semantic-release/exec \
    && npm install @semantic-release/git \
    && npm install @semantic-release/release-notes-generator \
    && npm install @semantic-release/changelog \
    && npm install @semantic-release/gitlab \
    && apk add --no-cache git openssh-client bash \
    && chmod +x /usr/local/bin/shipit;
